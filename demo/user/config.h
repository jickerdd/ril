/******************************************************************************
* @file     config.h
* @brief    ϵͳ����
* @version  1.0
* @date     2019-03-25
* @author   Morro
* 
* Coypright (C) 2018 
* All rights reserved.
*
*******************************************************************************/

#ifndef _CONFIG_H_
#define _CONFIG_H_

#define          APPLICATION_ADDRESS  0x8000     

extern const char * const project_name;         /* ��Ŀ����, �ַ��� */
extern const char * const mcu_part_number;      /* MCU�ͺ�,�ַ���,��"STM32F215RE" */
extern const char * const fmw_major_ver;        /* �̼����汾��(�鵵�汾��),�ַ��� */
extern const char * const fmw_minor_ver;        /* �̼��ΰ汾��(�����汾��),�ַ��� */
extern const char * const firmware_version;     /* �̼������汾��,�ַ��� */
extern const char * const company_name;         /* ��˾����, �ַ��� */
extern const char device_type;    


#endif
